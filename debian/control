Source: golang-github-mendersoftware-mender-artifact
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>, Lluis Campos <lluis.campos@northern.tech>
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-pkg-errors-dev,
               golang-github-stretchr-testify-dev,
               golang-github-urfave-cli-dev,
               golang-logrus-dev,
               golang-github-remyoudompheng-go-liblzma-dev,
               dosfstools <!nocheck>,
               e2fsprogs <!nocheck>,
               parted <!nocheck>,
               mtools <!nocheck>
Standards-Version: 4.2.1
Homepage: https://github.com/mendersoftware/mender-artifact
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-mendersoftware-mender-artifact
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-mendersoftware-mender-artifact.git
XS-Go-Import-Path: github.com/mendersoftware/mender-artifact
Testsuite: autopkgtest-pkg-go

Package: golang-github-mendersoftware-mender-artifact-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-pkg-errors-dev,
         golang-github-remyoudompheng-go-liblzma-dev,
         golang-github-stretchr-testify-dev,
         golang-github-urfave-cli-dev,
         golang-logrus-dev,
Breaks: mender-client (<< 2.2.0-2~)
Description: Library for managing Mender artifact files
 Mender Artifacts Library Mender is an open source over-the-air (OTA)
 software updater for embedded Linux devices. Mender comprises a client
 running at the embedded device, as well as a server that manages
 deployments across many devices.
 .
 This repository contains the artifacts library, which is used by the
 Mender client, command line interface, server and for build integration
 with the Yocto Project.
 .
 The artifacts library makes it easy to programmatically work with a
 Mender artifact, which is a file that can be recognized by its .mender
 suffix. Mender artifacts can contain binaries, metadata, checksums,
 signatures and scripts that are used during a deployment. The artifact
 format acts as a wrapper, and uses the tar format to bundle several
 files into one.
 .
 In its simplest form, an artifact contains just a rootfs image, along
 with its checksum, id and device type compatibility.
 .
 The artifacts library might also be useful for other updaters or
 purposes. We are always happy to see other uses of it!
 .
 Mender logo Getting started To start using Mender, we recommend that
 you begin with the Getting started section in the Mender documentation
 (https://docs.mender.io/).  Using the library You can use the parser
 and reader in go in the standard way:
 .
 import (
         "github.com/mendersoftware/mender-artifact/parser"
         "github.com/mendersoftware/mender-artifact/reader"
        )
 .
 For sample usage, please see the Mender client source code
 (https://github.com/mendersoftware/mender).

Package: mender-artifact
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: ${misc:Depends}, ${shlibs:Depends}, dosfstools, e2fsprogs, parted, mtools
Description: utility to generate .mender artifacts
 Mender Artifacts Library Mender is an open source over-the-air (OTA)
 software updater for embedded Linux devices. Mender comprises a client
 running at the embedded device, as well as a server that manages
 deployments across many devices.
 .
 This package contains the mender-artifact program which is used to generate
 the artifacts needed for mender OTA updates. The artifacts are generated
 from your existing ext4 rootfs image that you built for direct device
 deployments.
